﻿using BizBox.Api.Plugins.Interfaces;
using BizBox.TenantSystem.Interceptors;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;

namespace BizBox.TenantSystem;

/// <summary>
/// plugin for the tenant system
/// </summary>
public class TenantSystemPlugin : IPlugin
{
    /// <inheritdoc />
    public Task Configure(IApplicationBuilder builder, IConfiguration configuration)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public async Task ConfigureServices(IServiceCollection services, IConfiguration configuration)
    {
        services.AddTransient<ISaveChangesInterceptor, TenantSaveChangesInterceptor>();
    }

    /// <inheritdoc />
    public Task ConfigureServicesPost(IServiceCollection services, IConfiguration configuration)
    {
        throw new NotImplementedException();
    }
}