using BizBox.Api.Tenant.Exceptions;
using BizBox.Api.Tenant.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace BizBox.Infrastructure.Tenant.Interceptors;

/// <summary>
/// auto sets the tenant id
/// </summary>
public class TenantSaveChangesInterceptor : SaveChangesInterceptor
{
    private readonly ITenantAccessor _tenantAccessor;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="tenantAccessor"></param>
    public TenantSaveChangesInterceptor(ITenantAccessor tenantAccessor)
    {
        _tenantAccessor = tenantAccessor;
    }

    /// <inheritdoc />
    public override InterceptionResult<int> SavingChanges(DbContextEventData eventData, InterceptionResult<int> result)
    {
        UpdateEntities(eventData.Context);

        return base.SavingChanges(eventData, result);
    }

    /// <inheritdoc />
    public override ValueTask<InterceptionResult<int>> SavingChangesAsync(DbContextEventData eventData,
        InterceptionResult<int> result, CancellationToken cancellationToken = default)
    {
        UpdateEntities(eventData.Context);

        return base.SavingChangesAsync(eventData, result, cancellationToken);
    }

    private void UpdateEntities(DbContext? context)
    {
        if (context == null) return;

        foreach (var entry in context.ChangeTracker.Entries<ITenantObject>())
        {
            var tenant = _tenantAccessor.GetCurrentTenant();
            if (entry.State is EntityState.Added && entry.Entity.Tenant == default)
            {
                if (tenant is null) throw new TenantNotSetException();
                entry.Entity.Tenant = tenant.Id;
            }
        }
    }
}