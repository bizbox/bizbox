using BizBox.Api.Plugins.Dto;
using BizBox.Api.Plugins.Interfaces;

namespace BizBox.Infrastructure.Plugins;

/// <summary>
/// 
/// </summary>
public class PluginManagerService : IPluginManager
{
    private readonly List<PluginInstanceDto> _plugins = new();

    /// <inheritdoc />
    public bool IsInstalled(string id)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public void Load(string path)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public void Validate()
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public ICollection<PluginInstanceDto> LoadedPlugins => _plugins;
}