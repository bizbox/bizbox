﻿using BizBox.Api.Plugins.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BizBox.Keycloak;

/// <summary>
/// plugin for keycloak
/// </summary>
public class KeycloakPlugin : IPlugin
{
    /// <inheritdoc />
    public Task Configure(IApplicationBuilder builder, IConfiguration configuration)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task ConfigureServices(IServiceCollection services, IConfiguration configuration)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task ConfigureServicesPost(IServiceCollection services, IConfiguration configuration)
    {
        throw new NotImplementedException();
    }
}