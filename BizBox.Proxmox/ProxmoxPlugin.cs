using BizBox.Api.Plugins.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BizBox.Proxmox;

/// <summary>
/// plugin for proxmox
/// </summary>
public class ProxmoxPlugin : IPlugin
{
    /// <inheritdoc />
    public Task Configure(IApplicationBuilder builder, IConfiguration configuration)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task ConfigureServices(IServiceCollection services, IConfiguration configuration)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task ConfigureServicesPost(IServiceCollection services, IConfiguration configuration)
    {
        throw new NotImplementedException();
    }
}