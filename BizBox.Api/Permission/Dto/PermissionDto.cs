namespace BizBox.Api.Permission.Dto;

/// <summary>
/// 
/// </summary>
public class PermissionDto
{
    /// <summary>
    /// the permission id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// the service this permission belongs to 
    /// </summary>
    public required string Service { get; set; }

    /// <summary>
    /// the permission node
    /// </summary>
    public required string Permission { get; set; }

    /// <summary>
    /// what to display in the ui
    /// </summary>
    public string? Display { get; set; }
}