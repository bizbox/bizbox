using BizBox.Api.Group.Dto;
using BizBox.Api.Permission.Dto;
using BizBox.Api.User.Dto;

namespace BizBox.Api.Permission.Interfaces;

/// <summary>
/// the data store for permissions
/// </summary>
public interface IPermissionService
{
    /// <summary>
    /// gets all possible permission of the given service
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<IEnumerable<PermissionDto>> GetPermissions(CancellationToken cancellationToken = default);

    /// <summary>
    /// if the backend supports groups
    /// </summary>
    bool SupportsGroups { get; }

    /// <summary>
    /// adds a permission to a user
    /// </summary>
    /// <param name="user"></param>
    /// <param name="permissions"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<IEnumerable<PermissionDto>> AddPermission(UserDto user, IEnumerable<PermissionDto> permissions,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// removes a permission to a user
    /// </summary>
    /// <param name="user"></param>
    /// <param name="permissions"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<IEnumerable<PermissionDto>> RemovePermission(UserDto user, IEnumerable<PermissionDto> permissions,
        CancellationToken cancellationToken = default);


    /// <summary>
    /// adds a permission to a group
    /// </summary>
    /// <param name="group"></param>
    /// <param name="permissions"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<IEnumerable<PermissionDto>> AddPermission(GroupDto group, IEnumerable<PermissionDto> permissions,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// removes a permission to a group
    /// </summary>
    /// <param name="group"></param>
    /// <param name="permissions"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<IEnumerable<PermissionDto>> RemovePermission(GroupDto group, IEnumerable<PermissionDto> permissions,
        CancellationToken cancellationToken = default);
}