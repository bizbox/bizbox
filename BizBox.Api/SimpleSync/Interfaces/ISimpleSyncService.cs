using BizBox.Api.BaseService.Interfaces;
using BizBox.Api.Group.Dto;
using BizBox.Api.User.Dto;

namespace BizBox.Api.SimpleSync.Interfaces;

/// <summary>
/// a simple service to sync users and permissions
/// </summary>
public interface ISimpleSyncService : IBaseService
{
    /// <summary>
    /// do a full sync
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task Sync(CancellationToken cancellationToken = default);

    /// <summary>
    /// sync a user
    /// </summary>
    /// <param name="user"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task SyncUser(UserDto user, CancellationToken cancellationToken = default);

    /// <summary>
    /// sync a group
    /// </summary>
    /// <param name="group"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task SyncGroup(GroupDto group, CancellationToken cancellationToken = default);
}