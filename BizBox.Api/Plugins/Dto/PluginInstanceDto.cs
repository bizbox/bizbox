using System.Reflection;
using BizBox.Api.Plugins.Interfaces;
using BizBox.Api.Plugins.Settings;

namespace BizBox.Api.Plugins.Dto;

/// <summary>
/// stores data about a plugin
/// </summary>
public class PluginInstanceDto
{
    /// <summary>
    /// the assembly that stores the plugin
    /// </summary>
    public Assembly? Assembly { get; set; }

    /// <summary>
    /// all the entry points of the plugin
    /// </summary>
    public ICollection<IPlugin>? EntryPoints { get; set; }

    /// <summary>
    /// stores the plugins metadata
    /// </summary>
    public required PluginMetaData MetaData { get; set; }
}