namespace BizBox.Api.Plugins.Settings;

/// <summary>
/// MetaData for a plugin
/// </summary>
public class PluginMetaData
{
    /// <summary>
    /// the plugins id
    /// </summary>
    public required string Id { get; set; }

    /// <summary>
    /// other plugins this plugin can replace
    /// </summary>
    public required ICollection<string> Like { get; set; }

    /// <summary>
    /// the name of the plugin
    /// </summary>
    public required string Name { get; set; }

    /// <summary>
    /// the version of the plugin
    /// </summary>
    public required Version Version { get; set; }

    /// <summary>
    /// Dependencies of this plugin
    /// </summary>
    public required IList<PluginDependency> Dependency { get; set; }
}

/// <summary>
/// info about a dependency
/// </summary>
public class PluginDependency
{
    /// <summary>
    /// the dependent plugin Id
    /// </summary>
    public required string Id { get; set; }

    /// <summary>
    /// if this plugin will work without this
    /// </summary>
    public bool SoftDependency { get; set; }

    /// <summary>
    /// the version needed null for any
    /// </summary>
    public string? VersionString { get; set; }
}