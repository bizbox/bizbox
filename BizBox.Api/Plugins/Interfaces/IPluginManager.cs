using BizBox.Api.Plugins.Dto;
using BizBox.Api.Plugins.Settings;

namespace BizBox.Api.Plugins.Interfaces;

/// <summary>
/// manages plugins in the system
/// </summary>
public interface IPluginManager
{
    /// <summary>
    /// check if a plugin is installed
    /// </summary>
    /// <param name="id"></param>
    /// <returns>if a plugin with this Id or a plugin is claiming to be like it</returns>
    public bool IsInstalled(string id);

    /// <summary>
    /// load a plugin to be loaded
    /// </summary>
    /// <param name="path"></param>
    public void Load(string path);

    /// <summary>
    /// validates
    /// </summary>
    public void Validate();

    /// <summary>
    /// gets all the loaded plugins
    /// </summary>
    public ICollection<PluginInstanceDto> LoadedPlugins { get; }
}