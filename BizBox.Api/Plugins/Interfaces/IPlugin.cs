using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BizBox.Api.Plugins.Interfaces;

/// <summary>
/// the entry point for a addon
/// </summary>
public interface IPlugin
{
    /// <summary>
    /// load configs and set things up
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="configuration"></param>
    /// <returns></returns>
    Task Configure(IApplicationBuilder builder, IConfiguration configuration);

    /// <summary>
    /// main place to load services
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configuration"></param>
    /// <returns></returns>
    Task ConfigureServices(IServiceCollection services, IConfiguration configuration);

    /// <summary>
    /// load services after the main services have been loaded
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configuration"></param>
    /// <returns></returns>
    Task ConfigureServicesPost(IServiceCollection services, IConfiguration configuration);
}