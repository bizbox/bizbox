namespace BizBox.Api.Plugins.Attributes;

/// <summary>
/// 
/// </summary>
[AttributeUsage(AttributeTargets.Class)]
public class InternalPrefixAttribute : Attribute
{
}