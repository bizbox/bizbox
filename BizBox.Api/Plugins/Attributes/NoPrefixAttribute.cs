namespace BizBox.Api.Plugins.Attributes;

/// <summary>
/// you dont want the plugin id to be prepended to url
/// </summary>
[AttributeUsage(AttributeTargets.Class)]
public class NoPrefixAttribute : Attribute
{
}