namespace BizBox.Api;

/// <summary>
/// const for build in service ids
/// </summary>
public static class ServiceIds
{
    /// <summary>
    /// service for managing email services
    /// </summary>
    public static Guid MailService => Guid.Parse("061235bb-931a-4137-8e3e-000000000000");

    /// <summary>
    /// email backend service
    /// </summary>
    public static Guid MailBackendService => Guid.Parse("061235bb-931a-4137-8e3e-000000000001");
}