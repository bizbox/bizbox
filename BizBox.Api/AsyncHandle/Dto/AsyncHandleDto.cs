using BizBox.Api.Link.Dto;

namespace BizBox.Api.AsyncHandle.Dto;

/// <summary>
/// 
/// </summary>
public class AsyncHandleDto
{
    /// <summary>
    /// id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// if the task is still running
    /// </summary>
    public bool Completed { get; set; }

    /// <summary>
    /// when the data was last changed
    /// </summary>
    public DateTime LastChange { get; set; }

    /// <summary>
    /// the url to call to fetch the data
    /// </summary>
    public required LinkStreamDto Fetch { get; set; }

    /// <summary>
    /// url to stop the task if null then it cant be stopped
    /// </summary>
    public Uri? StopTask { get; set; }
}