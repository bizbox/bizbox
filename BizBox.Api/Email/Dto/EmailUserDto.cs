namespace BizBox.Api.Email.Dto;

/// <summary>
/// info about a users email account 
/// </summary>
public class EmailUserDto
{
    /// <summary>
    /// the BizBox Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// the id in the remote system
    /// </summary>
    public required string RemoteId { get; set; }

    /// <summary>
    /// the main email address
    /// </summary>
    public required string EmailAddress { get; set; }

    /// <summary>
    /// email alias
    /// </summary>
    public required IEnumerable<string> Alias { get; set; }

    /// <summary>
    /// info about the users email server
    /// </summary>
    public required EmailServerInfoDto ServerInfo { get; set; }
}