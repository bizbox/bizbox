namespace BizBox.Api.Email.Dto;

/// <summary>
/// info about a email domain
/// </summary>
public class EmailDomainDto
{
    /// <summary>
    /// the BizBox Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// the id in the remote system
    /// </summary>
    public string? RemoteId { get; set; }

    /// <summary>
    /// the main email address
    /// </summary>
    public required IList<string> Address { get; set; }

    /// <summary>
    /// email alias
    /// </summary>
    public required IList<string> Alias { get; set; }

    /// <summary>
    /// the id of the mail gateway used
    /// </summary>
    public Guid? Gateway { get; set; }

    /// <summary>
    /// the server email connection info
    /// </summary>
    public required EmailServerInfoDto ServerInfo { get; set; }
}