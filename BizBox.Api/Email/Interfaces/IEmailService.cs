using BizBox.Api.BaseService.Interfaces;
using BizBox.Api.Email.Dto;
using BizBox.Api.EmailGateway.Interfaces;

namespace BizBox.Api.Email.Interfaces;

/// <summary>
/// a service for a email service backend
/// </summary>
public interface IEmailService : IBaseService
{
    /// <summary>
    /// create a domain in the email system
    /// </summary>
    /// <param name="domain">the domain to add</param>
    /// <param name="gateway">if set use this gateway as next hop</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<EmailDomainDto> CreateDomain(string domain, IEmailGatewayService? gateway,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// list all the domains in this email service
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<IList<EmailDomainDto>> ListDomains(CancellationToken cancellationToken = default);
}