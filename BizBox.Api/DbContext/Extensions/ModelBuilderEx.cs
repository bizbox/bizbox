using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BizBox.Api.DbContext.Extensions;

/// <summary>
/// 
/// </summary>
public static class ModelBuilderEx
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="modelBuilder"></param>
    /// <param name="buildAction"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static ModelBuilder EntitiesOfType<T>(this ModelBuilder modelBuilder,
        Action<EntityTypeBuilder> buildAction) where T : class
    {
        return modelBuilder.EntitiesOfType(typeof(T), buildAction);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="modelBuilder"></param>
    /// <param name="type"></param>
    /// <param name="buildAction"></param>
    /// <returns></returns>
    public static ModelBuilder EntitiesOfType(this ModelBuilder modelBuilder, Type type,
        Action<EntityTypeBuilder> buildAction)
    {
        foreach (var entityType in modelBuilder.Model.GetEntityTypes()
                     .Where(entityType => type.IsAssignableFrom(entityType.ClrType)))
            buildAction(modelBuilder.Entity(entityType.ClrType));

        return modelBuilder;
    }
}