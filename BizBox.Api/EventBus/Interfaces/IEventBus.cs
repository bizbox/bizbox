using BizBox.Api.EventBus.Events;

namespace BizBox.Api.EventBus.Interfaces;

/// <summary>
/// an event bus
/// </summary>
public interface IEventBus
{
    /// <summary>
    /// publish an event to the bus
    /// </summary>
    /// <param name="event"></param>
    /// <returns></returns>
    Task Publish<T>(T @event) where T : BaseEvent;

    /// <summary>
    /// sub to an event
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TH"></typeparam>
    void Subscribe<T, TH>()
        where T : BaseEvent
        where TH : IEventHandler<T>;

    /// <summary>
    /// unsub to an event
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TH"></typeparam>
    void Unsubscribe<T, TH>()
        where TH : IEventHandler<T>
        where T : BaseEvent;
}