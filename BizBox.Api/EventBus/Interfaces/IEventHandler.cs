using BizBox.Api.EventBus.Events;

namespace BizBox.Api.EventBus.Interfaces;

/// <summary>
/// 
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IEventHandler<in T> where T : BaseEvent
{
    /// <summary>
    /// handel the event
    /// </summary>
    /// <param name="payload"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task Handel(T payload, CancellationToken cancellationToken);
}