using BizBox.Api.Tenant.Interfaces;

namespace BizBox.Api.EventBus.Events;

/// <summary>
/// baseEvent
/// </summary>
public class BaseEvent : ITenantObject
{
    /// <inheritdoc />
    public Guid Tenant { get; set; }
}