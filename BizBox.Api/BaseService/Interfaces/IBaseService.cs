using BizBox.Api.Permission.Interfaces;

namespace BizBox.Api.BaseService.Interfaces;

/// <summary>
/// a base interface for all service types
/// </summary>
public interface IBaseService
{
    /// <summary>
    /// get the permission service for this service
    /// </summary>
    /// <returns></returns>
    public IPermissionService GetPermissionService();
}