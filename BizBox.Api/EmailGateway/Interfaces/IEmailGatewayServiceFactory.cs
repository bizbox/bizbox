using BizBox.Api.BaseService.Interfaces;
using BizBox.Api.Email.Interfaces;
using BizBox.Api.EmailGateway.Enums;

namespace BizBox.Api.EmailGateway.Interfaces;

/// <summary>
/// a service for a email gateway service backend
/// </summary>
public interface IEmailGatewayServiceFactory : IBaseService
{
    /// <summary>
    /// get a email service backend by email
    /// </summary>
    /// <param name="emailAddress"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public IEmailService GetEmailServiceByEmail(
        string emailAddress,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// get email service by domain
    /// </summary>
    /// <param name="domain"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public IEmailService GetEmailServiceByDomain(
        string domain,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// gets all backends grouped by domain
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Dictionary<string, IEnumerable<IEmailService>> GetAll(CancellationToken cancellationToken = default);
}