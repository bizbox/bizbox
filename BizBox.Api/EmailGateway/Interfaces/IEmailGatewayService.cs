using BizBox.Api.BaseService.Interfaces;
using BizBox.Api.Email.Interfaces;
using BizBox.Api.EmailGateway.Enums;

namespace BizBox.Api.EmailGateway.Interfaces;

/// <summary>
/// a service for a email gateway service backend
/// </summary>
public interface IEmailGatewayService : IEmailService
{
    /// <summary>
    /// what direction the gateway can be used
    /// </summary>
    public EmailGatewayDirection Direction { get; }
}