namespace BizBox.Api.EmailGateway.Enums;

/// <summary>
/// the direction 
/// </summary>
[Flags]
public enum EmailGatewayDirection
{
    /// <summary>
    /// the gateway can be used for inbound email
    /// </summary>
    InBound,

    /// <summary>
    /// the gateway can be used for outbound email
    /// </summary>
    OutBound,

    /// <summary>
    /// the gateway can be used for both inbound and outbound email
    /// </summary>
    Both = InBound | OutBound
}