namespace BizBox.Api.EmailGateway.Dto;

/// <summary>
/// info about a backend gate way
/// </summary>
public class EmailGatewayServerInfoDto
{
    /// <summary>
    /// the BizBox Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// the id in the remote system
    /// </summary>
    public string? RemoteId { get; set; }

    /// <summary>
    /// a display name
    /// </summary>
    public required string DisplayName { get; set; }

    /// <summary>
    /// the address to connect to
    /// </summary>
    public required string Address { get; set; }

    /// <summary>
    /// the SMTP port
    /// </summary>
    public ushort? SMTP { get; set; }

    /// <summary>
    /// the SMTPS port
    /// </summary>
    public ushort? SMTPS { get; set; }

    /// <summary>
    /// the IMAP port
    /// </summary>
    public ushort? IMAP { get; set; }

    /// <summary>
    /// the IMAPS port
    /// </summary>
    public ushort? IMAPS { get; set; }

    /// <summary>
    /// the POP3 port
    /// </summary>
    public ushort? POP { get; set; }

    /// <summary>
    /// the POP3S port
    /// </summary>
    public ushort? POPS { get; set; }
}