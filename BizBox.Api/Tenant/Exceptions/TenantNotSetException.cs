using BizBox.Api.Common.Exceptions;

namespace BizBox.Api.Tenant.Exceptions;

/// <summary>
/// 
/// </summary>
public class TenantNotSetException : BizBoxException
{
    /// <inheritdoc />
    public TenantNotSetException()
    {
    }

    /// <inheritdoc />
    public TenantNotSetException(string? message) : base(message)
    {
    }

    /// <inheritdoc />
    public TenantNotSetException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}