namespace BizBox.Api.Tenant.Dto;

/// <summary>
/// tenant data
/// </summary>
public class TenantDto
{
    /// <summary>
    /// 
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// the display name
    /// </summary>
    public required string DisplayName { get; set; }
}