using System.Linq.Expressions;
using BizBox.Api.DbContext.Extensions;
using BizBox.Api.Tenant.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BizBox.Api.Tenant.Extensions;

/// <summary>
/// Extensions for use in db context
/// </summary>
public static class DbContextEx
{
    /// <summary>
    /// adds the tenant system to the current dbcontext
    /// </summary>
    /// <param name="modelBuilder"></param>
    /// <param name="tenantAccessor"></param>
    public static void AddTenantSystem(this ModelBuilder modelBuilder, ITenantAccessor tenantAccessor)
    {
        var tenant = tenantAccessor.GetCurrentTenant();
        if (tenant is null) return;

        modelBuilder.EntitiesOfType<ITenantObject>(builder =>
        {
            // query filters :)
            var param = Expression.Parameter(builder.Metadata.ClrType, nameof(ITenantObject.Tenant));
            var body = Expression.Equal(Expression.Property(param, nameof(ITenantObject.Tenant)),
                Expression.Constant(tenant.Id));
            builder.HasQueryFilter(Expression.Lambda(body, param));
        });
    }
}