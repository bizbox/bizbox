namespace BizBox.Api.Tenant.Interfaces;

/// <summary>
/// an object that is scoped to a Tenant
/// </summary>
public interface ITenantObject
{
    /// <summary>
    /// the tenant id
    /// </summary>
    public Guid Tenant { get; set; }
}