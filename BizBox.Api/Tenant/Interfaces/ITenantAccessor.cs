using BizBox.Api.Tenant.Dto;

namespace BizBox.Api.Tenant.Interfaces;

/// <summary>
/// get the tenant for the current request
/// </summary>
public interface ITenantAccessor
{
    /// <summary>
    /// get the current tenant
    /// </summary>
    /// <returns></returns>
    TenantDto? GetCurrentTenant();
}