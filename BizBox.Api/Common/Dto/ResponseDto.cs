namespace BizBox.Api.Common.Dto;

/// <summary>
/// a response with date
/// </summary>
/// <typeparam name="T"></typeparam>
public class ResponseDto<T> : ResponseDto
{
    /// <summary>
    /// the data payload
    /// </summary>
    public T? Data { get; set; }
}

/// <summary>
/// the base response
/// </summary>
public class ResponseDto
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="errors"></param>
    public ResponseDto(List<ProblemDto> errors)
    {
        Errors = errors;
    }

    /// <summary>
    /// 
    /// </summary>
    public ResponseDto()
    {
        Errors = new List<ProblemDto>();
    }

    /// <summary>
    /// any errors that happen in the request
    /// </summary>
    public List<ProblemDto> Errors { get; set; }

    /// <summary>
    /// no issues and no data to return
    /// </summary>
    /// <returns></returns>
    public static ResponseDto Ok()
    {
        return new ResponseDto();
    }
}