namespace BizBox.Api.Common.Dto;

/// <summary>
/// a list object
/// </summary>
public class ListDto<TDto>
{
    /// <summary>
    /// items
    /// </summary>
    public required IEnumerable<TDto> Items { get; set; }

    /// <summary>
    /// total items
    /// </summary>
    public uint Total { get; set; }

    /// <summary>
    /// the index offset
    /// </summary>
    public uint Offset { get; set; }

    /// <summary>
    /// the count in Items
    /// </summary>
    public uint Count { get; set; }
}