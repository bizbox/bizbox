using System.Runtime.Serialization;

namespace BizBox.Api.Common.Exceptions;

/// <summary>
/// base Exception for BizBox
/// </summary>
public class BizBoxException : Exception
{
    /// <inheritdoc />
    public BizBoxException()
    {
    }

    /// <inheritdoc />
    public BizBoxException(string? message) : base(message)
    {
    }

    /// <inheritdoc />
    public BizBoxException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}