using BizBox.Api.Permission.Dto;
using BizBox.Api.Tenant.Interfaces;

namespace BizBox.Api.Group.Dto;

/// <summary>
/// 
/// </summary>
public class GroupDto : ITenantObject
{
    /// <summary>
    /// group id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// the permissions of a group
    /// </summary>
    public required IEnumerable<PermissionDto> Permissions { get; set; }

    /// <inheritdoc />
    public Guid Tenant { get; set; }
}