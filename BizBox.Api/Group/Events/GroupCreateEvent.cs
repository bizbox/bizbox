using BizBox.Api.EventBus.Events;
using BizBox.Api.Group.Dto;
using BizBox.Api.User.Dto;

namespace BizBox.Api.Group.Events;

/// <summary>
/// event for when a new group is added to the system
/// </summary>
public class GroupCreateEvent : BaseEvent
{
    /// <summary>
    /// the group payload
    /// </summary>
    public required GroupDto Group { get; set; }
}