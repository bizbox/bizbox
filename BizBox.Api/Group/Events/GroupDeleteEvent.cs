using BizBox.Api.EventBus.Events;
using BizBox.Api.Group.Dto;

namespace BizBox.Api.Group.Events;

/// <summary>
/// event for when a new group is removed from the system
/// </summary>
public class GroupDeleteEvent : BaseEvent
{
    /// <summary>
    /// the group payload
    /// </summary>
    public required GroupDto Group { get; set; }
}