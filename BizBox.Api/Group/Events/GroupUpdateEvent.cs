using BizBox.Api.EventBus.Events;
using BizBox.Api.Group.Dto;

namespace BizBox.Api.Group.Events;

/// <summary>
/// event for when a new group is updated to the system
/// </summary>
public class GroupUpdateEvent : BaseEvent
{
    /// <summary>
    /// the group payload
    /// </summary>
    public required GroupDto Group { get; set; }
}