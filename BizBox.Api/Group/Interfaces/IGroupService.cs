using System.Linq.Expressions;
using BizBox.Api.Group.Dto;

namespace BizBox.Api.Group.Interfaces;

/// <summary>
/// service for managing groups
/// </summary>
public interface IGroupService
{
    /// <summary>
    /// adds a group
    /// </summary>
    /// <param name="group"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task AddGroup(GroupDto group, CancellationToken cancellationToken = default);

    /// <summary>
    /// remove a group
    /// </summary>
    /// <param name="group"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task RemoveGroup(GroupDto group, CancellationToken cancellationToken = default);

    /// <summary>
    /// updates a group
    /// </summary>
    /// <param name="id"></param>
    /// <param name="group"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task UpdateGroup(Guid id, GroupDto group, CancellationToken cancellationToken = default);

    /// <summary>
    /// loop over all the groups
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public IAsyncEnumerable<GroupDto> GetGroups(CancellationToken cancellationToken = default);

    /// <summary>
    /// loop over all the groups with this filter
    /// </summary>
    /// <param name="filter"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public IAsyncEnumerable<GroupDto> GetGroups(Expression<GroupDto> filter,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// gets all the groups in the list
    /// </summary>
    /// <param name="groupIds"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public ICollection<GroupDto> GetGroups(IEnumerable<Guid> groupIds, CancellationToken cancellationToken = default);
}