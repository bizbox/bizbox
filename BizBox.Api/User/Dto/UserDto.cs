using BizBox.Api.Permission.Dto;
using BizBox.Api.Tenant.Interfaces;

namespace BizBox.Api.User.Dto;

/// <summary>
/// store user data
/// </summary>
public class UserDto : ITenantObject
{
    /// <summary>
    /// the user id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// the users display name
    /// </summary>
    public string DisplayName { get; set; } = null!;

    /// <summary>
    /// mobile number for TOTP and the like
    /// </summary>
    public string MobileNumber { get; set; } = null!;

    /// <summary>
    /// users email address
    /// </summary>
    public string Email { get; set; } = null!;

    /// <summary>
    /// if the user is active or not
    /// </summary>
    public bool Activate { get; set; }

    /// <summary>
    /// the permissions that belong to the user
    /// </summary>
    public IEnumerable<PermissionDto> Permission { get; set; } = null!;

    /// <summary>
    /// list of group id's
    /// </summary>
    public IEnumerable<Guid> Groups { get; set; } = null!;

    /// <inheritdoc />
    public Guid Tenant { get; set; }
}