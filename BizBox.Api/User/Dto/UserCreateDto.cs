using BizBox.Api.Permission.Dto;
using BizBox.Api.Tenant.Interfaces;

namespace BizBox.Api.User.Dto;

/// <summary>
/// create a user
/// </summary>
public class UserCreateDto
{
    /// <summary>
    /// the users display name
    /// </summary>
    public required string DisplayName { get; set; }

    /// <summary>
    /// mobile number for TOTP and the like
    /// </summary>
    public string? MobileNumber { get; set; }

    /// <summary>
    /// users email address
    /// </summary>
    public required string Email { get; set; }

    /// <summary>
    /// the permissions that belong to the user
    /// </summary>
    public IEnumerable<string> Permission { get; set; } = null!;

    /// <summary>
    /// list of group id's
    /// </summary>
    public IEnumerable<Guid> Groups { get; set; } = null!;
}