using System.Linq.Expressions;
using BizBox.Api.User.Dto;

namespace BizBox.Api.User.Interfaces;

/// <summary>
/// service for managing users
/// </summary>
public interface IUserService
{
    /// <summary>
    /// adds a user
    /// </summary>
    /// <param name="user"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task AddUser(UserDto user, CancellationToken cancellationToken = default);

    /// <summary>
    /// remove a user
    /// </summary>
    /// <param name="user"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task RemoveUser(UserDto user, CancellationToken cancellationToken = default);

    /// <summary>
    /// updates a user
    /// </summary>
    /// <param name="id"></param>
    /// <param name="user"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task UpdateUser(Guid id, UserDto user, CancellationToken cancellationToken = default);

    /// <summary>
    /// loop over all the users
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public IAsyncEnumerable<UserDto> GetUsers(CancellationToken cancellationToken = default);

    /// <summary>
    /// loop over all the user with this filter
    /// </summary>
    /// <param name="filter"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public IAsyncEnumerable<UserDto>
        GetUsers(Expression<UserDto> filter, CancellationToken cancellationToken = default);

    /// <summary>
    /// gets all the users in the list
    /// </summary>
    /// <param name="userIds"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public ICollection<UserDto> GetUsers(IEnumerable<Guid> userIds, CancellationToken cancellationToken = default);
}