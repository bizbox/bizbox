using System.Net;
using BizBox.Api.Common.Dto;
using BizBox.Api.User.Dto;
using Microsoft.AspNetCore.Mvc;

namespace BizBox.Api.User.Interfaces;

/// <summary>
/// interface for user endpoints
/// </summary>
public interface IUserController
{
    /// <summary>
    /// gets a list of the users
    /// </summary>
    /// <param name="offset"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    [HttpGet("users")]
    public Task<ResponseDto<ListDto<UserDto>>> ListUsers(uint offset = 0, uint count = 10);

    /// <summary>
    /// creates a user
    /// </summary>
    /// <returns></returns>
    [HttpPost("user")]
    [ProducesResponseType(typeof(ResponseDto<UserDto>), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ResponseDto), (int)HttpStatusCode.Forbidden)]
    public Task<ResponseDto<UserDto>> CreateUser([FromBody] UserCreateDto user);

    /// <summary>
    /// gets a user
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [HttpGet("user/{userId:guid}")]
    [ProducesResponseType(typeof(ResponseDto<UserDto>), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ResponseDto), (int)HttpStatusCode.NotFound)]
    public Task<ResponseDto<UserDto>> GetUser(Guid userId);

    /// <summary>
    /// activates a user
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [HttpGet("user/{userId:guid}/activate")]
    [ProducesResponseType(typeof(ResponseDto), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ResponseDto), (int)HttpStatusCode.NotFound)]
    public Task<ResponseDto> ActivateUser(Guid userId);

    /// <summary>
    /// de-activates a user
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [HttpGet("user/{userId:guid}/de-activate")]
    [ProducesResponseType(typeof(ResponseDto), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ResponseDto), (int)HttpStatusCode.NotFound)]
    public Task<ResponseDto> DeActivateUser(Guid userId);

    /// <summary>
    /// removes a user
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [HttpDelete("user/{userId:guid}")]
    [ProducesResponseType(typeof(ResponseDto), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ResponseDto), (int)HttpStatusCode.NotFound)]
    public Task<ResponseDto> DeleteUser(Guid userId);
}