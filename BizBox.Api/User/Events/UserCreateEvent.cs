using BizBox.Api.EventBus.Events;
using BizBox.Api.User.Dto;

namespace BizBox.Api.User.Events;

/// <summary>
/// event for when a new uer is added to the system
/// </summary>
public class UserCreateEvent : BaseEvent
{
    /// <summary>
    /// the user payload
    /// </summary>
    public required UserDto User { get; set; }
}