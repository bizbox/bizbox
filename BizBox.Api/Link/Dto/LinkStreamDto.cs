namespace BizBox.Api.Link.Dto;

/// <summary>
/// a link to stream data
/// </summary>
public class LinkStreamDto
{
    /// <summary>
    /// start of the stream
    /// </summary>
    public required Uri Start { get; set; }

    /// <summary>
    /// a ws link to the live stream
    /// </summary>
    public Uri? Live { get; set; }
}