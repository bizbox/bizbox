using BizBox.Api.Plugins.Attributes;
using BizBox.Api.Plugins.Interfaces;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace BizBox.Core.Web.Plugin;

/// <summary>
/// 
/// </summary>
public class ApiPrefixConvention : IApplicationModelConvention
{
    private readonly IPluginManager _pluginManager;

    /// <summary>
    /// 
    /// </summary>
    public ApiPrefixConvention(IPluginManager pluginManager)
    {
        _pluginManager = pluginManager;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="application"></param>
    public void Apply(ApplicationModel application)
    {
        // Loop through any controller matching our selector
        foreach (var controller in application.Controllers)
            // Either update existing route attributes or add a new one
            if (controller.Selectors.Any(x => x.AttributeRouteModel != null))
                AddPrefixesToExistingRoutes(controller);
            else
                AddNewRoute(controller);
    }

    private string GetPrefix(ControllerModel controllerModel)
    {
        var url = _pluginManager.LoadedPlugins
            .FirstOrDefault(
                i => i.Assembly == controllerModel.ControllerType.Assembly
            )?.MetaData?.Id ?? "com.punksky.bizbox";
        return controllerModel
            .Attributes
            .Any(i => i.GetType() == typeof(InternalPrefixAttribute))
            ? $"internal/{url}"
            : url;
    }

    private AttributeRouteModel GetPrefixAttributeRouteModel(ControllerModel controllerModel,
        AttributeRouteModel? current = null)
    {
        var fallback = new AttributeRouteModel(
            new RouteAttribute($"api/{GetPrefix(controllerModel)}/[controller]")
        );
        if (current is null)
        {
            return fallback;
        }
        else
        {
            var prefix = new AttributeRouteModel(
                new RouteAttribute($"api/{GetPrefix(controllerModel)}/")
            );
            return AttributeRouteModel
                .CombineAttributeRouteModel(prefix, current) ?? fallback;
        }
    }

    private void AddPrefixesToExistingRoutes(ControllerModel controller)
    {
        foreach (var selectorModel in controller.Selectors.Where(x => x.AttributeRouteModel != null).ToList())
        {
            // Merge existing route models with the api prefix
            var originalAttributeRoute = selectorModel.AttributeRouteModel;
            selectorModel.AttributeRouteModel = GetPrefixAttributeRouteModel(controller, originalAttributeRoute);
        }
    }

    private void AddNewRoute(ControllerModel controller)
    {
        // The controller has no route attributes, lets add a default api convention 
        var defaultSelector = controller.Selectors.First(s => s.AttributeRouteModel == null);
        defaultSelector.AttributeRouteModel = GetPrefixAttributeRouteModel(controller);
    }
}