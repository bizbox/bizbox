using System.Reflection;
using BizBox.Api.Plugins.Interfaces;
using BizBox.Core.Web.Plugin;
using BizBox.Infrastructure.Plugins;
using BizBox.Infrastructure.Tenant.Interceptors;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;

var pluginManager = new PluginManagerService();

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddSingleton<IPluginManager>(pluginManager);

builder.Services.AddTransient<ISaveChangesInterceptor, TenantSaveChangesInterceptor>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(opt =>
{
    opt.SwaggerDoc("v1",
        new OpenApiInfo()
        {
            Title = "BizBox Api",
            Version = "v1"
        }
    );
    
});

builder.Services.AddMvcCore(opt => { opt.Conventions.Add(new ApiPrefixConvention(pluginManager)); });

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(opt =>
{
    
});


app.Run();