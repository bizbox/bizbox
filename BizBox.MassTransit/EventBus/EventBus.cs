using BizBox.Api.EventBus.Events;
using BizBox.Api.EventBus.Interfaces;
using MassTransit;

namespace BizBox.MassTransit.EventBus;

/// <summary>
/// 
/// </summary>
public class EventBus : IEventBus
{
    private readonly ISendEndpointProvider _bus;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="bus"></param>
    public EventBus(ISendEndpointProvider bus)
    {
        _bus = bus;
    }

    /// <inheritdoc />
    public async Task Publish<T>(T @event) where T : BaseEvent
    {
        var key = typeof(T).Name;
        var endpoint = await _bus.GetSendEndpoint(new Uri(@"queue:{key}"));
        await endpoint.Send<T>(@event);
    }

    /// <inheritdoc />
    public void Subscribe<T, TH>() where T : BaseEvent where TH : IEventHandler<T>
    {
        var key = typeof(T).Name;
    }

    /// <inheritdoc />
    public void Unsubscribe<T, TH>() where T : BaseEvent where TH : IEventHandler<T>
    {
        var key = typeof(T).Name;
    }
}