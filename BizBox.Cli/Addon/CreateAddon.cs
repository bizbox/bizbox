using Spectre.Console.Cli;

namespace BizBox.Cli.Addon;

/// <summary>
/// make a new addon
/// </summary>
public class CreateAddonCommand : Command<CreateAddonCommand.Settings>
{
    /// <summary>
    /// command settings
    /// </summary>
    public class Settings : CommandSettings
    {
        /// <summary>
        /// name of addon
        /// </summary>
        [CommandArgument(0, "[Name]")]
        public required string Name { get; set; }
    }


    /// <inheritdoc />
    public override int Execute(CommandContext context, Settings settings)
    {
        throw new NotImplementedException();
    }
}