﻿// See https://aka.ms/new-console-template for more information

using BizBox.Cli.Addon;
using Spectre.Console.Cli;

var app = new CommandApp();
app.Configure(config =>
{
    config.AddBranch("plugin", pluginCommand =>
    {
        pluginCommand.AddCommand<CreateAddonCommand>("create")
            .WithAlias("new");
    });
});