using BizBox.Api.EventBus.Interfaces;
using BizBox.Api.User.Events;

namespace BizBox.scim.EventHandles;

/// <summary>
/// 
/// </summary>
public class UserCreateHandle : IEventHandler<UserCreateEvent>
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="payload"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    public async Task Handel(UserCreateEvent payload, CancellationToken cancellationToken)
    {
    }
}