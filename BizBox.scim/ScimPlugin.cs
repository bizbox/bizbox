﻿using BizBox.Api.EventBus.Interfaces;
using BizBox.Api.Plugins.Interfaces;
using BizBox.Api.User.Events;
using BizBox.scim.EventHandles;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BizBox.scim;

/// <summary>
/// scim plugin entry point
/// </summary>
public class ScimPlugin : IPlugin
{
    /// <inheritdoc />
    public async Task Configure(IApplicationBuilder builder, IConfiguration configuration)
    {
        var eventBus = builder.ApplicationServices.GetRequiredService<IEventBus>();
        eventBus.Subscribe<UserCreateEvent, UserCreateHandle>();
    }

    /// <inheritdoc />
    public Task ConfigureServices(IServiceCollection services, IConfiguration configuration)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task ConfigureServicesPost(IServiceCollection services, IConfiguration configuration)
    {
        throw new NotImplementedException();
    }
}