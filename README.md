# BizBox

BizBox is a integration suite that manages users groups and permissions a cross different applications

The goal is the allow business's to have more freedom with the software they use by making it simple to manage 
their users across all platforms they use and to allow some automation based on events

We do not allow the use of software that is under a Source Available Licence in this project any PR that includes with
will be rejected if there is not a oss implementation that can work as a drop in replacement.

## Feature Requests

when it comes to feature requests you have a few options

* Make an issue in gitlab requesting the change. The change will be made if someone wants to make it
* Make your own pull request.
* Pay someone to do it for you.

## Support

The support you get it the support you pay for.

We may offer support contracts later down the road.

## Road Map

### MileStone 1
* KeyCloak as a Auth Backend
* Proxmox
  * User Group and Permission management
  * Auto configure OIDC 
* Gitlab
  * User and Group management
* Tenant login screen
  * use BizBox to display the login screen to allow the tenant system to work

### Planed

* OpenLDAP
  * auth backend?
  * User Group and Permission management?
* nginx proxy manager
  * User management
* SCIM Server
  * Allow BizBox to act as a SCIM server
* Mail Cow 
  * User management and basic onboarding automation
* Powerdns Admin
  * User and Group management
* AWS
  * User Group and Permission management
* Azure
  * User Group and Permission management
* OVH
  * User Group and Permission management

## License

Unless stated all projects are under AGPL v3

BizBox.Api is under LGPL